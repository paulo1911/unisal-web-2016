// Aqui nós carregamos o gulp e os plugins através da função `require` do nodejs
var gulp = require('gulp');
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');
var browserSync = require('browser-sync');
var del = require('del');

// Definimos o diretorio dos arquivos para evitar repetição futuramente
var JS_FILES = "./src/js/*.js";
// Definimos o diretorio dos arquivos para evitar repetição futuramente
var CSS_FILES = "./src/css/*.css";
// Definimos o diretorio dos arquivos para evitar repetição futuramente
var HTML_FILES = "./src/*.html";

var WHATCH_FILES = __dirname + '/src';

//limpar a pasta de destino
gulp.task('limpar', function () {
	
	console.log('Limpando pasta: '+ __dirname + '/build');

 	del.sync(__dirname + '/build/**');
});

//Builda o Html
gulp.task('html', function () {

    console.log('building HTML files');

    gulp.src(HTML_FILES)
        .pipe(gulp.dest('./build'))
});

//Builda o css
gulp.task('css', function () {

	console.log('Building CSS');

    gulp.src(CSS_FILES)
        .pipe(gulp.dest('./build/css'))
});


//Aqui criamos uma nova tarefa através do ´gulp.task´ e damos a ela o nome 'lint'
gulp.task('js', function() {

    console.log('building JS');

    // Carregamos os arquivos novamente
    // E rodamos uma tarefa para concatenação
    // Renomeamos o arquivo que sera minificado e logo depois o minificamos com o `uglify`
    // E pra terminar usamos o `gulp.dest` para colocar os arquivos concatenados e minificados na pasta build/
    gulp.src(JS_FILES)
    .pipe(uglify())
    .pipe(gulp.dest('./build/js'));
});

gulp.task("libs", function () {
    gulp.src("./node_modules/bootstrap/dist/**").pipe(gulp.dest("./build/lib/bootstrap"));
    gulp.src("./node_modules/jquery/dist/**").pipe(gulp.dest("./build/lib/jquery"));
    gulp.src("./node_modules/angular/**").pipe(gulp.dest("./build/lib/angular"));
});

//Criamos uma tarefa 'default' que vai rodar quando rodamos `gulp` no projeto
gulp.task('default', ['limpar'], function() {


// Usamos o `gulp.run` para rodar as tarefas
// E usamos o `gulp.watch` para o Gulp esperar mudanças nos arquivos para rodar novamente
gulp.run('html','css', 'js', 'libs');

	   gulp.watch(WHATCH_FILES, function(evt) {
		  gulp.run('js','html', 'css', 'libs');
	   });
});



//Synch using browser synch with proxy
gulp.task('server', function() {
	console.log('Running server');
    browserSync.init(['./build/**','./build/css/**','./build/js/**' ,'./build/*.html'], {
        server: {
            baseDir: './build',
            index: 'index_angular.html'
        }
    });
});
