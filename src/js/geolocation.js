function getLocation() {
	console.log('Getting geolocation...');
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(showPosition);
        console.log('Calling geolocation API...');
    } else {
    	console.log('Geolocation not supported');
        $('#geolocationMessage').show();
    }
    console.log('End Get geolocation...');
}
function showPosition(position) {
	console.log(' Latitude: '+ position.coords.latitude);
	$('#txtLatitude').val(position.coords.latitude);

	console.log(' Longitude: '+ position.coords.longitude);
	$('#txtLongitude').val(position.coords.longitude);
	console.log($('#txtLatitude').val() + '/' + $('#txtLongitude').val() );
	console.log('Get Back geolocation...');
}

(function () {
   	console.log('Start geolocation script...');
   	$('#geolocationMessage').hide();
	getLocation();
	console.log('End geolocation script...');
})();
